# light-organ-firmware

## what

This repository contains the functional firmware for my [light-organ project](http://kelly-dunn.me/blog/light-organ)

## usage

This project is intended to make use of the Teensyduino and PJRC Audio library.  You can learn more about how to setup those development enviroments by clicking the links below:

- [Teensyduino](https://www.pjrc.com/teensy/td_download.html) 
- [PJRC Audio Library](https://github.com/PaulStoffregen/Audio)

## installation

Verifying, Compiling, and Uploading this firmware as an arduino script to your Teensy 3.1 device through the Arduino interface is the best way of installing the latest and greatest version of this firmware.


