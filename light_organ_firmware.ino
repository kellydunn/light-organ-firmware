#include <Audio.h>
#include <Wire.h>
#include <SD.h>
 
// The canonical input device for the light-organ audio circuit.
const int MIC = AUDIO_INPUT_MIC;

// The number of FFT magnitude bins in which to average
// when calculating the pwm duty cycle of a given LED strip.
const int BIN_SIZE=16;

// Hardcoded maximum average magnitudes that are used to drive 
// the LED channels to full brightness
const int BIN_MAX[8] = {
  4500,
  1500,
  700,
  700,
  550,
  525,
  475,
  400
};

// Hardcoded minimum average magnitudes that are used to
// effectively drive the LED Channels low to no brightness.
const int BIN_MIN[8] = {
  2000,
  700,
  200,
  100,
  100,
  10,
  5,
  0
};

// The number of LED Strips this firmware is intended to drive
const int NUM_LED_STRIPS = 7;

// The pins that are writing PWM signals to the LED channels in order,
// Lowest Frequency bins to Higher Frequency bins.
const int PINS[NUM_LED_STRIPS] = {
  20,
  21,
  10,
  6,
  5,
  4,
  3
};

// Create the Audio components.  These should be created in the
// order data flows, inputs/sources -> processing -> outputs
//
AudioInputI2S       audioInput;         // audio shield: mic or line-in
AudioAnalyzeFFT256  myFFT(20);
AudioOutputI2S      audioOutput;        // audio shield: headphones & line-out

// Create Audio connections between the components
//
AudioConnection c1(audioInput, 0, audioOutput, 0);
AudioConnection c2(audioInput, 0, myFFT, 0);
AudioConnection c3(audioInput, 1, audioOutput, 1);

// Create an object to control the audio shield.
// 
AudioControlSGTL5000 audioShield;

void setup() {
  // Audio connections require memory to work.  For more
  // detailed information, see the MemoryAndCpuUsage example
  AudioMemory(12);

  // Enable the audio shield and set the output volume.
  audioShield.enable();
  audioShield.inputSelect(MIC);
  audioShield.volume(0.6);
  
  for(int i =0; i < NUM_LED_STRIPS; i++) {
    pinMode(PINS[i], OUTPUT); 
  }
}

void loop() {

  if (myFFT.available()) {

    // each time new FFT data is available
    // print it all to the Arduino Serial Monitor
    Serial.print("FFT: ");
    for (int i=0; i<8; i++) {
      
      int sample = 0;
      for(int j=0; j < BIN_SIZE; j++) {
        sample += myFFT.output[(i*BIN_SIZE) + j];
        Serial.print(myFFT.output[i]);
        Serial.print(", ");
      }
      
      int avg = sample / BIN_SIZE;
      float pct = 1.0;

      // Calculate the magnitude in which we will drive an LED Channel      
      if(avg < BIN_MIN[i]) {
        pct = 0.0;
      } else {
        pct = ((1.0 *avg) - BIN_MIN[i]) / (BIN_MAX[i] - BIN_MIN[i]);
      }
      
      // Write the duty cycle to the corresponding LED Channel
      if(i < NUM_LED_STRIPS) {
        analogWrite(PINS[i], pct * 0xFF); 
      }
    }

    Serial.println();
  }
}